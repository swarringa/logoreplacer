import java.nio.file.Paths

trait TxaTestSupport {

    final List<String> openBedrijfsGevens = '''\
        [SOURCE]
        PROPERTY:BEGIN
        PRIORITY 8001
        PROPERTY:END
        !Bedrijfsgegevens
        CLEAR(BED:record)
        BED:Bedrijfsnummer = 1
        Access:Bedrijfsgegevens.Fetch(BED:KeyBedrijfsnummer)\
    '''.split('\n')*.trim()

    final List<String> setupLogo = '''\
        [SOURCE]
        PROPERTY:BEGIN
        PRIORITY 5001
        PROPERTY:END
        !Logo
        Settarget(Report)
        ?Image1{Prop:Text}   = BED:Logo
        ?Image1{Prop:Height} = BED:Hoog
        ?Image1{Prop:Width}  = BED:Breed
        SetTarget()\
    '''.split('\n')*.trim()

    final List<String> windowManagerCodeSectionEmbedPoint = '''\
        EMBED %WindowManagerMethodCodeSection
        [INSTANCES]\
    '''.split('\n')*.trim()

    final List<String> windowInitEmbedPoint = '''\
        WHEN 'Init'
        [INSTANCES]
        WHEN '(),BYTE'
        [DEFINITION]\
    '''.split('\n')*.trim()

    final List<String> openReportEmbedPoint = '''\
        WHEN 'OpenReport'
        [INSTANCES]
        WHEN '(),BYTE'
        [DEFINITION]\
    '''.split('\n')*.trim()
    def assertStructuresAtLine(Procedure p, int lineno, List<String>... structures){
        def n = lineno
        for (s in structures){
            assertStructureAtLine(p, n, s)
            n += s.size()
        }
    }

    def assertStructureAtLine(Procedure p, int lineno, structure){
        def lines = p.body.toString().toLineArray()
        assert lines.size() > 0
        assert lines.size() >= lineno + structure.size()
        def bodySection = lines[lineno..lineno+structure.size()-1]
        assert bodySection*.trim() == structure*.trim()
    }

    def assertSectionsClosedCorrectly(Procedure p) {
        assertSectionsClosedCorrectly(p.body.toString())
    }

    def assertSectionsClosedCorrectly(String s){
        def level = 0
        for (line in s.toLineArray()*.trim()){
            if(line.isSectionMark()){
                SectionMark section = line.asSectionMark()
                if ( section.requiresExplicitEnd()){
                    level++
                } else if ( section == SectionMark.END){
                    level--
                }
            }
        }
        assert level == 0
    }

    def assertAllSectionsWithoutIndents(Procedure p){
        def unalignedSections = p.body.toString().toLineArray().findAll { it ==~ /^\s+\[[A-Z]+\]\s*$/}
        assert unalignedSections.size() == 0
    }

    File openTxaFile(String resourcePath){
        def resourceURL = getClass().getClassLoader().getResource(resourcePath)
        return new File(Paths.get(resourceURL.toURI()).toString())
    }
}