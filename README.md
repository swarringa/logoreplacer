# UDEA Ticket 48662 - vervangen logo

Bulk update van een applicatie TXA waarbij de harde koppeling naar 'udea'gif' in 
report logos wordt vervangen door instelling vanuit de tabel Bedrijfsgegevens.

De programmatuur doet het volgende:
  
*	Selectie van aan te passen procedures: reports met een image control gekoppeld aan “udea.gif”
*	Voor elke procedure (voor zover nodig):
    *	Toevoegen van bestand bedrijfsgegevens als deze nog niet als primary, secondary of other bestand is opgenomen
    *	Indien bestand bedrijfsgegevens toegevoegd is een source embed toevoegen aan de windowmanager (report manager)/ init procedure
    *	Logo instellingen vanuit bestand bedrijfsgegevens aanbrengen op logo image control via een source embed  point in windowmanager / OpenReport procedure
    *	Verwijderen “udea.gif”van logo image control

## Build procedure

De programmatuur maakt gebruik van gradle. Gebruik de standaard gradle wrapper om de programmatuur te bouwen.

`.\gradlew clean fatJar`

## Werkwijze

### 1. Exporteer TXA
Clarion: Exporteer de volledige applicatie vanuit clarion als  TXA-bestand via menu Applicatie > Export application to text.

### 2. Draai het conversie script
Open een windows command prompt en voer het batch script uit als volgt:

    `replacelogo.bat pad\naar\txa\bestand`

Er is nu een map exports aangemaakt met daarin een subfolder met dezelfde naam als de txa. 
In deze subfolder staan txa’s voor elke aangepaste procedure.
Er is ook een txa beginnend met 48862_ waarin alle aangepaste procedures zijn opgenomen

### 3. Importeer de gegenereerde txa('s) 

Open in clarion dezelfde .app file als in stap 1.  importeer nu de gewenste TXA('s) via menu Applicatie > Import txa. 
Clarion geeft een melding dat er een name-clash is tussen de te importeren procedures en de bestaande procedures.
Kies voor de optie "Vervang all". 

Als je er voor kiest de 48662_...txa te importeren (dus alle procedures in 1 keer) dan worden de geimporteerde procedures
in een aparte module geplaatst. Dit maakt het eenvoudiger ze te vinden ter controle van de aanpassingen.

## Kwown errors

Prijsbestel.app: 
* bij importeren van procedure PrintAanbodAGFenKaas worden de formules niet correct geimporteerd. Dit is een clarion bug. Deze ie gerapporteerd aan softvelocity.
