import java.nio.file.Files
import java.nio.file.Paths
import java.time.LocalDateTime
import java.util.regex.Matcher
import java.util.logging.Logger
import static SectionMark.*

/**
 * Udea ticket 48862 : vervangen logo
 */
class LogoReplacer {

    private static final EOL = System.lineSeparator()

    private static Logger logger

    static {
        System.setProperty("java.util.logging.SimpleFormatter.format",
                '[%1$tF %1$tT] [%4$-7s] %5$s %n')
        logger = Logger.getLogger("LogoReplacer")
        ClarionStringMixins.initialize()
        ClarionDateMixins.initialize()
    }

    /**
     * Class to collect information about the processed content
     * while reading the content line-by-line which guides the
     * modification logic.
     */
    class ParserState {
        // Current line number being read
        Long currentLineNr = null

        // Current line contents
        String currentLine = null

        // Last section mark that was read
        String lastSectionMark = null

        // Holds the current %embedpoint defined by EMBED %embedpoint
        def currentEmbedPoint = null

        // is increased on every WHEN ... inside EMBED
        def instanceLevel = 0

        // Stores the name in WHEN '<name>' to figure out in which
        // instance of the embed-point we are. Can hold an array
        // because WHEN statements can be nested
        def currentEmbedInstance = []

        // Flags to keep track of relevant positions within the TXA structure
        def insidePrimary = false
        def insideSecondary = false
        def insideOthers = false
        def insideEmbedsSection = false
        def insideDefinition = false
        def insideReport = false
        def insideSource = false
        def insidePrompts = false

        // Custom property map
        def props = [:]
    }

    /**
     * Perform logo replacement on a specific procedure. The procedure's body is processed
     * (physical) line by line and passed for transformation to the different hook methods.
     *
     * @param procedure - Procedure instance. The procedure's body is modified as a side-effect
     */
    ParserState replaceProcedureLogo(Procedure procedure) {
        def txaReader = new StringReader(procedure.body.toString())
        def txaWriter = new StringWriter(procedure.body.length())

        // Parser state collects information during processing
        ParserState ps = new ParserState()

        // Current line of txa being processed, 1-based
        ps.currentLineNr = procedure.lineNumber

        logger.info "Vervangen logo voor procedure ${procedure.name} @ ${procedure.lineNumber}"

        // Allow for custom properties to be set before processing starts
        processInit(ps)

        // Note: tranformLine will automatically put an EOL after each transformed line!
        // Therefore only add appropriate EOL's when line is transformed into multiple lines
        // otherwise Clarion import may fail
        txaReader.transformLine(txaWriter) { line ->

            ps.currentLine = line

            // Holds the transformed line. By default the current line is written out
            def tl = line


            if (line.isSectionMark()) {
                def section = line.asSectionMark()
                if (section == END) {
                    def content = processSectionEnd(ps)
                    // [END] of [EMBED], [INSTANCE] or [DEFINITION]
                    updateEmbedPointInfo(ps, section)
                    if (content != null && !content.isEmpty()) {
                        tl = content + EOL + section
                    }
                } else {
                    logger.finer  "Entering section ${section} at line ${ps.currentLineNr}"

                    tl = ""

                    def contentPre = processSectionTransition(ps, section)
                    if (contentPre != null && !contentPre.trim().isEmpty()) {
                        tl += contentPre + EOL
                    }

                    tl += section.tag

                    // [INSTANCE], [DEFINITION]
                    updateEmbedPointInfo(ps, section)

                    def contentPost = processSectionStart(ps, section)
                    if (contentPost != null && !contentPost.trim().isEmpty()) {
                        tl += EOL + contentPost
                    }

                    ps.lastSectionMark = section
                }
            } else {
                // WHEN <embedpoint-instance>
                updateEmbedPointInfo(ps)

                tl = processSectionContent(ps)
            }

            ps.currentLineNr++

            // Just as a precaution remove any superflous EOL's to prevent
            // blank lines that may break Clarion import
            if ( tl.endsWith(EOL)){
                tl.trimEOL()
            }

            return tl
        }

        procedure.body = new StringBuilder(txaWriter.toString().trimEOL())
        return ps
    }

    /**
     * Update the current embed point information based on the current line/section
     * @param ps the current parser state
     * @param section the section mark on the current line, null if not not a section mark
     * @return nothing, parser state is updated as side-effect
     */
    def updateEmbedPointInfo(ParserState ps, section=null){
        // EMBED embedpoint
        def embedPointPattern = /^\s*EMBED\s+(%?\w+)\s*/

        // WHEN 'embedinstance'
        def embedInstancePattern = /^\s*WHEN\s+'([\(\)\,\w]+)'\s*/

        if ( section == null) {
            /* Keeping track of the relevant embed points */
            if (ps.insideEmbedsSection ) {
                Matcher m = ps.currentLine =~ embedPointPattern
                if (m.matches()) {
                    ps.currentEmbedPoint = m[0][1] as String
                    ps.currentEmbedInstance = []
                    ps.instanceLevel = 0
                }

                m = ps.currentLine =~ embedInstancePattern
                if (m.matches()) {
                    if (ps.instanceLevel <= ps.currentEmbedInstance.size()) {
                        // Sibling WHEN within [INSTANCE] section
                        ps.currentEmbedInstance = ps.currentEmbedInstance.take(ps.instanceLevel)
                        ps.currentEmbedInstance[ps.instanceLevel - 1] = m[0][1] as String
                    } else {
                        // Child WHEN for nested [INSTANCE]
                        ps.currentEmbedInstance.add(m[0][1] as String)
                    }
                }
            }
        } else {
            switch (section) {
            case EMBED:
                ps.insideEmbedsSection = true
                ps.insidePrompts = false
                break
            case INSTANCES:
                ps.instanceLevel += 1
                break
            case DEFINITION:
                ps.insideDefinition = true
                break
            case SOURCE:
                ps.insideSource = true
                break
            case PROMPTS:
                ps.insidePrompts = true
                break
            case END:
                /* The sections [EMBEDS], [INSTANCES] and [DEFINITION] are all terminated by
                 * an explicit [END]. Based on the flags that tell us where in the EMBED structure
                 * we currently are we modify the flags to indicate the new location.
                 * NOTE: the order in which we perform the checks is from lowest level to highest level
                 * so at every [END] the flag at the correct level is deactivated.
                 */
                if (ps.insideDefinition) {
                    // [END] of [DEFINITION]
                    ps.insideDefinition = false
                    if (ps.instanceLevel == 0) {
                        // EMBED without [INSTANCE]'s
                        ps.currentEmbedPoint = null
                        ps.currentEmbedInstance = []
                    }
                } else {
                    // [END] of [INSTANCES]
                    if (ps.instanceLevel > 1) {
                        if (ps.currentEmbedInstance.size() > 0) {
                            ps.currentEmbedInstance.pop()
                            ps.instanceLevel--
                        }
                    } else {
                        // Dropping out of the embed point
                        ps.currentEmbedInstance = []
                        ps.currentEmbedPoint = null
                        ps.instanceLevel = 0
                    }
                }

                ps.insideSource = false
                break

            default:
                ps.insideSource = false
            }
        }
    }

    /**
     * Hook to setup parser state before processing starts
     * @param ps
     * @return
     */
    void processInit(ParserState ps){
        ps.props.with {
            hasPrimaryBedrijfsGegevens = false
            hasSecondaryBedrijfsGegevens = false
            hasOtherBedrijfsGegevens = false
            hasBedrijfsgegevensFetch = false
            printsFromQueue = false

            // Keeps track of operations performed on TXA
            openFileWasInserted = false
            logoSetupWasInserted = false
        }
    }

    /**
     * Processes a single line of content within the current section
     * @param ps - parser state holding processing information of previous content
     * @param line - the current line read from the txa
     * @return the transformed line
     */
    String processSectionContent(ParserState ps) {
        // By default the line read is written out unchanged
        def content = ps.currentLine

        final modifiedDatePattern = /^\s*MODIFIED\s+[0-9\/:\s']+$/
        final reportSourcePattern = /^\s*%ReportDataSource\s+DEFAULT\s+\('(\w+)'\)\s*$/

        /* Detect presence of Bedrijfsgegevens file */
        if (ps.insidePrimary && ps.currentLine.startsWith('Bedrijfsgegevens')) {
            logger.info "PRIMARY: bestand Bedrijfsgegevens al aanwezig"
            ps.props.hasPrimaryBedrijfsGegevens = true
        } else if (ps.insideSecondary && ps.currentLine.contains('Bedrijfsgegevens')) {
            logger.info "SECONDARY: bestand Bedrijfsgegevens al aanwezig"
            ps.props.hasSecondaryBedrijfsGegevens = true
        } else if (ps.insideOthers && ps.currentLine.startsWith('Bedrijfsgegevens')) {
            logger.info "OTHER: bestand Bedrijfsgegevens al aanwezig"
            ps.props.hasOtherBedrijfsGegevens = true
        }

        /* Check sourcecode for fetching the bedrijfsgegevens record */
        if ( ps.insideSource && ps.currentLine.contains('Access:Bedrijfsgegevens.Fetch')){
            ps.props.hasBedrijfsgegevensFetch = true
        }

        /* Check additions for report source */
        if ( ps.insidePrompts && ps.currentLine ==~ reportSourcePattern){
            ps.props.printsFromQueue = ((ps.currentLine =~ reportSourcePattern)[0][1] as String).equalsIgnoreCase('queue')
        }

        /* Remove udea.gif from any IMAGE control inside the [REPORT] section */
        if (ps.insideReport && ps.currentLine.toLowerCase().indexOf("image('udea.gif')") > 0) {
            logger.info "Verwijderen 'udea.gif' van image"
            content = ps.currentLine.replaceFirst(/IMAGE\('udea.gif'\)/, 'IMAGE')
        }

        /* Set modification date of procedure to this moment */
        if (ps.currentLine ==~ modifiedDatePattern){
            content = "MODIFIED " + LocalDateTime.now().toClarionDateTimeString()
        }

        return content
    }

    /**
     * Most sections are implicitly ended by the next section. At such a transition checks
     * can be performed to see what info was or was not present in the section and insert
     * some content at the end of the section.
     *
     * @param ParserState - state holding information about the processed section
     * @param nextSection - next section reader is entering
     * @return content to be inserted
     */
    String processSectionTransition(ParserState ps, SectionMark nextSection) {
        def content = ""

        switch (nextSection) {
            case PRIMARY:
                ps.insidePrimary = true
                break
            case SECONDARY:
                ps.insideSecondary = true
                break
            case OTHERS:
                ps.insidePrimary = false
                ps.insideSecondary = false
                ps.insideOthers = true
                break
            case REPORT:
                ps.insideReport = true
                break
            case DEFINITION:
                ps.insideDefinition = true
                break
            case PROMPTS:
                /*
                 * The [PROMPTS] section will implicitly terminate the FILES section of
                 * the procedure. At this point we need to check if we the need to add the
                 * [OTHERS] section with the Bedrijfsgegevens file or add just the file
                 * to the existing [OTHERS] section
                 */
                if ( ps.insidePrimary || ps.insideSecondary || ps.insideOthers){
                    content = insertOrUpdateOtherFiles(ps)
                }
                ps.insidePrimary = false
                ps.insideSecondary = false
                ps.insideOthers = false
                break
            case EMBED:
                ps.insideEmbedsSection  = true
                break
        }

        return content
    }

    /**
     * This hook provides inserting content right AFTER the start of a section
     * and/or modify the parser state
     * @param ps - current parser state
     * @param section - the new section
     * @return
     */
    String processSectionStart(ParserState ps, SectionMark section) {
        def content = ""

        switch (section) {
            case DEFINITION:
                content = processEmbedDefinitionSectionStart(ps)
        }

        return content
    }

    /**
     * Some sections are explicitly ended by [END]. Ths hook the opportunity to insert
     * content before the [END] mark and to modify the parser state for subsequent
     * processing
     *
     * @param ps - the current parser state
     * @return content to insert before the section [END] mark
     */
    String processSectionEnd(ParserState ps) {
        String content = ""

        if (ps.insideEmbedsSection ) {
            /*
             * If our embeds were not inserted at the start of the DEFINITION section by
             * processEmbedDefinitionSectionStart the embed point instance location(s)
             * are not present. Actually he whole embed point may not be present. In all
             * these case the correct structure needs to be created before the appropriate
             * [END] mark
             */
            if (ps.currentEmbedPoint == '%WindowManagerMethodCodeSection') {
                if (!ps.props.logoSetupWasInserted || !ps.props.openFileWasInserted) {
                    content += insertSourceEmbeds(ps)
                }
            } else if ( ps.currentEmbedPoint == null ) {
                // We're inside the [EMBED] section but have no active embed point. The current [END]
                // marks the end of the [EMBED] section and if nothing has been inserted the
                // %WindowManagerMethodCodeSection apparently does not exist. Therefore create it and
                // insert our embeds

                if (!ps.props.logoSetupWasInserted || needsBedrijfsGegevensInsert(ps.props)) {
                    content += '''\
                        EMBED %WindowManagerMethodCodeSection
                        [INSTANCES]
                        '''.stripIndent()

                    content += insertSourceEmbeds(ps) + EOL

                    content += "[END]" + EOL
                }
            } else {
                // Other embed-point : ignore
            }
        }

        return content
    }

    /**
     * Creates or updates the [OTHERS] section with the Bedrijfsgegevens file
     *
     * @param ps
     * @param line
     * @return
     */
    String insertOrUpdateOtherFiles(ParserState ps) {
        String content = ""

        // Encountered [PROMPTS] without encountering [OTHERS] -> [OTHERS] section is not there
        if ( ps.currentLine.isSectionStart(PROMPTS)) {
            if ((ps.insidePrimary && !ps.props.hasPrimaryBedrijfsGegevens) ||
                    (ps.insideSecondary && !(ps.props.hasPrimaryBedrijfsGegevens || ps.props.hasSecondaryBedrijfsgegevens))) {

                logger.info "Toevoegen other files sectie met bestand BedrijfsGegevens"
                content = '''\
                    [OTHERS]
                    Bedrijfsgegevens\
                '''
            } else if (ps.insideOthers && !ps.props.hasPrimaryBedrijfsGegevens && !ps.props.hasOtherBedrijfsGegevens) {
                logger.info "BedrijfsGegevens toevoegen aan other files"
                content = "Bedrijfsgegevens"
            }
        }

        return content.stripIndent()
    }

    /**
     * Entering the definition section of an embed point instance
     *
     * @param ps
     * @return content to be inserted
     */
    String processEmbedDefinitionSectionStart(ParserState ps) {
        /*
         * Start of the template code generation definitions inside an embed point instance.
         * Here we need to insert our source embeds depending on the embed point we're in
         */

        def content = ""

        if (ps.currentEmbedPoint == '%WindowManagerMethodCodeSection' && ps.currentEmbedInstance == ['OpenReport', '(),BYTE']) {
            logger.info "Source embed toevoegen voor instellen logo op regel ${ps.currentLineNr}"
            content = insertSetupLogo()
            ps.props.logoSetupWasInserted = true
        }

        return content.stripIndent()
    }

    /**
     * Check to see if embed for opening the Bedrijfsgegevens file needs to be inserted.
     * - When the report data source is a file (not a queue) and the Bedrijfsgegevens file was already declared then
     * we'll assume the record be fetched from the generated source code.
     * - If the data source is a file but the Bedrijfsgegevens file was not declared we'll need to add a source embed, unless
     * there's already a source embed that fetches the record.
     * - If the report data source is a queue then even when the Bedrijfsgegevens file has been declared we'll add the embed
     * to open the file unless it was already fetched by another source embed.
     **/
    def needsBedrijfsGegevensInsert = {props ->
        def bedrijfsGegevensDeclared = props.hasPrimaryBedrijfsgegevens || props.hasSecondaryBedrijfsGegevens || props.hasOtherBedrijfsGegevens
        return !props.openFileWasInserted && !props.hasBedrijfsgegevensFetch && ( props.printsFromQueue || !bedrijfsGegevensDeclared )
    }

    /**
     * Inserts the source embed instances with (if necessary) their required surrounding
     * embed instance structure.
     *
     * @param ps
     * @return
     */
    def insertSourceEmbeds(ParserState ps) {
        def openFileContent = ""
        def logoContent = ""

        if ( ps.insideEmbedsSection ) {

            if (needsBedrijfsGegevensInsert(ps.props)) {
                if (ps.currentEmbedInstance == ['Init', '(),BYTE']) {
                    logger.info "Toevoegen openen bedrijfsgegevens aan bestaande embed instance"
                    openFileContent += insertOpenBedrijfsgegevens()
                    ps.props.openFileWasInserted = true
                } else if (ps.currentEmbedInstance == ['Init']) {
                    logger.info "Toevoegen openen bedrijfsgegevens aan ingevoegde instance (),BYTE"
                    openFileContent += insertOpenBedrijfsgegevens(insertVirtual: true)
                    ps.props.openFileWasInserted = true
                } else if (ps.currentEmbedInstance == []) {
                    logger.info "Toevoegen openen bedrijfsgegevens aan ingevoegde instance Init / (),BYTE"
                    openFileContent += insertOpenBedrijfsgegevens(insertWindowInit: true)
                    ps.props.openFileWasInserted = true
                } else if ( ps.currentEmbedPoint == '%WindowManagerMethodCodeSection' && !ps.insideDefinition && ps.instanceLevel == 1 && ps.currentLine.isSectionEnd()){
                    // End of the %WindowManagerMethodCodeSection [INSTANCES]
                    openFileContent += insertOpenBedrijfsgegevens(insertWindowInit: true)
                    ps.props.openFileWasInserted = true
                }
            }

            if (!ps.props.logoSetupWasInserted) {

                if (ps.currentEmbedInstance == ['OpenReport', '(),BYTE']) {
                    logger.info "Toevoegen logo setup aan bestaande instance"
                    logoContent += insertSetupLogo()
                    ps.props.logoSetupWasInserted = true
                } else if (ps.currentEmbedInstance == ['OpenReport']) {
                    logger.info "Toevoegen logo setup aan ingevoegde instance (),BYTE"
                    logoContent += insertSetupLogo(insertVirtual: true)
                    ps.props.logoSetupWasInserted = true
                } else if (ps.currentEmbedInstance == []) {
                    // End of [EMBED]
                    logger.info "Toevoegen logo setup aan ingevoegde instance OpenReport / (),BYTE"
                    logoContent += insertSetupLogo(insertOpenReport: true)
                    ps.props.logoSetupWasInserted = true
                } else if ( ps.currentEmbedPoint == '%WindowManagerMethodCodeSection' && !ps.insideDefinition && ps.instanceLevel == 1 && ps.currentLine.isSectionEnd()){
                    // End of the %WindowManagerMethodCodeSection [INSTANCES]
                    logoContent += insertSetupLogo(insertOpenReport: true)
                    ps.props.logoSetupWasInserted = true
                }
            }
        }

        def content = openFileContent
        content += content.size() > 0 && logoContent.size() > 0 ? EOL : ""
        content += logoContent

        return content
    }

    /**
     * Insert the source embed definition for opening the Bedrijfsegevens file
     * at window initialization
     *
     * @param insertWindowInit - also insert surrounding init embed instance
     * @param insertVirtual - also insert surrounding virtual procedure instance
     * @return nothing
     */
    def insertOpenBedrijfsgegevens(insertWindowInit = false, insertVirtual = false) {
        final embedSource = '''\
        [SOURCE]
        PROPERTY:BEGIN
        PRIORITY 8001
        PROPERTY:END
        !Bedrijfsgegevens
        CLEAR(BED:record)
        BED:Bedrijfsnummer = 1
        Access:Bedrijfsgegevens.Fetch(BED:KeyBedrijfsnummer)\
        '''

        if (insertWindowInit) {
            """\
        WHEN 'Init'
        [INSTANCES]
        WHEN '(),BYTE'
        [DEFINITION]
        ${embedSource}
        [END]
        [END]\
       """.stripIndent()
        } else if (insertVirtual) {
            """\
        WHEN '(),BYTE'
        [DEFINITION]
        ${embedSource}
        [END]\
        """.stripIndent()
        } else {
            embedSource.stripIndent()
        }
    }

    /**
     * Insert the source embed definition for setting up the logo from
     * Bedrijfsgegevens fields when report is opened
     *
     * @param insertOpenReport - also insert surrounding open report embed instance
     * @param insertVirtual - also insert surrounding virtual procedure instance
     * @return nothing
     */
    def insertSetupLogo(insertOpenReport = false, insertVirtual = false) {

        final embedSource = """[SOURCE]
        PROPERTY:BEGIN
        PRIORITY 5001
        PROPERTY:END
        !Logo
        Settarget(Report)
        ?Image1{Prop:Text}   = BED:Logo
        ?Image1{Prop:Height} = BED:Hoog
        ?Image1{Prop:Width}  = BED:Breed
        SetTarget()\
    """

        if (insertOpenReport) {
            """\
        WHEN 'OpenReport'
        [INSTANCES]
        WHEN '(),BYTE'
        [DEFINITION]
        ${embedSource}
        [END]
        [END]\
        """.stripIndent()
        } else if (insertVirtual) {
            """\
        WHEN '(),BYTE'
        [DEFINITION]
        ${embedSource}
        [END]\
        """.stripIndent()
        } else {
            embedSource.stripIndent()
        }
    }

    /**
     * Check if procedure needs to be processed. For this ticket we only need to
     * process reports with report IMAGE linked to file "udea.gif"
     * @param p a procedure instance
     * @return true if it needs to be processed
     */
    boolean needsLogoReplacement(Procedure p) {
        if (p.template && !p.template.equalsIgnoreCase("ABC Report")) {
            return false
        } else {
            return p.body.indexOf("IMAGE('udea.gif')") > 0
        }
    }

    def run(options) {
        final def txaPreamble = '''\
            [MODULE]
            [COMMON]
            FROM ABC GENERATED
        '''.stripIndent()

        final def txaClosing = '[END]'

        new File(options.targetfile).withWriter { newTxa ->
            newTxa << txaPreamble
            new TxaExtractor(options.sourcefile).extractProcedures().each { procedure ->
                if (needsLogoReplacement(procedure)) {
                    replaceProcedureLogo(procedure)
                    newTxa <<  procedure.body << EOL

                    if ( options.extract){
                        procedure.save(Paths.get(options.outputdir).resolve(procedure.name + '.txa').toString())
                    }
                }
            }
            newTxa << EOL << txaClosing
        }
    }

    static void main(String... args){
        def cli = new CliBuilder(usage: 'replace-logo [options] /path/to/app.txa')

        cli.with {
            o(longOpt: 'output' , 'Output folder for writing txa files. Defaults to current directory.', args:1)
            s(longOpt: 'create-source-folder', 'Create a separate source folder in output named after input txa')
            p(longOpt: 'prefix', 'Use specified prefix for generated files (default is 48862)')
            x(longOpt: 'extract', 'Write all modified procedures as single txa files to output folder')
        }

        def options = cli.parse(args)
        if (!options || args.size() < 1) {
            cli.usage()
            return
        }

        def currentDir = Paths.get('.').toAbsolutePath().normalize()
        def userHome = System.getProperty("user.home")

        def outputDir = options.o ? options.o : '.'
        def txaFilePath = args.last()
        def prefix = options.p ? options.p as String : '48862'
        def extract = options.x ? true : false


        if ( txaFilePath.startsWith('~')){
            txaFilePath = txaFilePath.replaceFirst('~', userHome)
        }

        txaFilePath = Paths.get(txaFilePath).toAbsolutePath().normalize()

        if (!Files.exists(txaFilePath)) {
            println("TXA file not found: " + txaFilePath.toString())
            return
        }

        if ( outputDir.startsWith('~')){
            outputDir = outputDir.replaceFirst('~',userHome)
        }

        outputDir = Paths.get(outputDir).toAbsolutePath().normalize()

        if ( Files.exists(outputDir) ){
            if ( !Files.isDirectory(outputDir) ) {
                println("Output ${outputDir} is a file not a folder!")
                return
            }
        }

        def txaBaseName
        def sourceFolder = txaFilePath.getFileName().toString()
        def extIdx = sourceFolder.reverse().indexOf('.')
        if ( extIdx > 0 ) {
            txaBaseName = sourceFolder.reverse().substring(extIdx+1).reverse()
        } else {
            txaBaseName = sourceFolder
        }

        if ( options.s ) {
            outputDir = outputDir.resolve(txaBaseName).toAbsolutePath().normalize().toString()
        }

        if ( !new File(outputDir).exists() ){
            if ( !new File(outputDir).mkdirs()){
                println("Failed to create output directory: " + outputDir.toString())
                return
            }
        }

        new LogoReplacer().run([
            sourcefile: txaFilePath.toString(),
            targetfile: Paths.get(outputDir.toString()).resolve(prefix + '_' + txaBaseName + '.txa').toAbsolutePath().normalize().toString(),
            outputdir: outputDir.toString(),
            txabasename: txaBaseName,
            prefix: prefix,
            extract: extract
        ])
    }

}
