/**
 * Deze test suite bevat testen voor procedures die problemen gaven bij
 * automatische aanpassing en is bedoeld om regressie te detecteren.
 */
class UdeaRegressionTest extends GroovyTestCase implements TxaTestSupport {
    void setUp() throws java . lang . Exception {
        super.setUp()
        ClarionStringMixins.initialize()
        ClarionDateMixins.initialize()
    }

    void testPrintHistorieProductPeriodeTotProduct(){
        def contents= '''\
        [EMBED]
        EMBED %ProcessManagerMethodCodeSection
        [INSTANCES]
        WHEN 'TakeRecord'
        [INSTANCES]
        WHEN '(),BYTE'
        [DEFINITION]
        [SOURCE]
        PROPERTY:BEGIN
        PRIORITY 5500
        PROPERTY:END
        !Filter
        IF (HVER:DatumLeveren => GloVanDatum and HVER:DatumLeveren =< GloTotDatum)
        !IF (HVER:DatumLeveren => GloVanDatum and HVER:DatumLeveren =< GloTotDatum) and |
                !   (HVER:ProductGroep => GloVanProductgroep and HVER:ProductGroep =< GloTotProductgroep)
        PRINT(RPT:Historie)
        END
        [END]
        [END]
        [END]
        [END]\
        '''.stripIndent()

        def p = new Procedure()
        p.name = "Test"
        p.body = new StringBuilder(contents)
        assertSectionsClosedCorrectly(p)

        def lr = new LogoReplacer()
        lr.replaceProcedureLogo(p)
        assertSectionsClosedCorrectly(p)

        assert p.body.contains('!Bedrijfsgegevens')
        assert p.body.contains('!Logo')
    }

    /**
     * Duplicate [OTHERS] was added for this procedure
     */
    void testPrintOmzetTotaalOmzetBWMetConsumentenprijs(){
        def txaFile = openTxaFile("PrintOmzetTotaalOmzetBWMetConsumentenprijs.txa")

        def p = new Procedure()
        p.name = "Test"
        p.body = new StringBuilder(txaFile.text)
        assertSectionsClosedCorrectly(p)

        def lr = new LogoReplacer()
        LogoReplacer.ParserState ps  = lr.replaceProcedureLogo(p)
        assertSectionsClosedCorrectly(p)
        assertAllSectionsWithoutIndents(p)

        ps.props.with {
            assert printsFromQueue == false
            assert hasPrimaryBedrijfsGegevens == false
            assert hasSecondaryBedrijfsGegevens == false
            assert hasOtherBedrijfsGegevens == true
            assert hasBedrijfsgegevensFetch == true
            assert openFileWasInserted == false
            assert logoSetupWasInserted == true
        }

        assert p.body.findAll(/\[OTHERS\]/).size() == 1
        assert p.body.findAll('EMBED %WindowManagerMethodCodeSection').size() == 1
        assert p.body.contains('!Logo')
    }


    void testPrintVergelijkProductgroepenInkoop(){
        def txaFile = openTxaFile("PrintVergelijkProductgroepenInkoop.txa")

        def p = new Procedure()
        p.name = "Test"
        p.body = new StringBuilder(txaFile.text)
        assertSectionsClosedCorrectly(p)

        def lr = new LogoReplacer()
        LogoReplacer.ParserState ps = lr.replaceProcedureLogo(p)
        assertSectionsClosedCorrectly(p)
        assertAllSectionsWithoutIndents(p)
        ps.props.with {
            assert printsFromQueue == false
            assert hasPrimaryBedrijfsGegevens == true
            assert hasSecondaryBedrijfsGegevens == false
            assert hasOtherBedrijfsGegevens == false
            assert hasBedrijfsgegevensFetch == false
            assert openFileWasInserted == true
            assert logoSetupWasInserted == true
        }
    }


    /**
     * Bug: embed for opening the bedrijfsgegevens file was not added
     * because the Bedrijfsgegevens file was declared as a primary file and
     * thus assumed to be openened. However, this procedure prints from queue
     * not from file, and does not contain code to open the file via the access manager.
     */
    void testPrintBeyondBioAfdelingPeriode(){
        def txaFile = openTxaFile("PrintBeyondBioAfdelingPeriode.txa")

        def p = new Procedure()
        p.name = "Test"
        p.body = new StringBuilder(txaFile.text)
        assertSectionsClosedCorrectly(p)

        def lr = new LogoReplacer()
        LogoReplacer.ParserState ps  = lr.replaceProcedureLogo(p)
        assertSectionsClosedCorrectly(p)
        assertAllSectionsWithoutIndents(p)

        ps.props.with {
            assert printsFromQueue == true
            assert hasPrimaryBedrijfsGegevens == true
            assert hasSecondaryBedrijfsGegevens == false
            assert hasOtherBedrijfsGegevens == false
            assert hasBedrijfsgegevensFetch == false
            assert openFileWasInserted == true
            assert logoSetupWasInserted == true
        }

        assert p.body.findAll('EMBED %WindowManagerMethodCodeSection').size() == 1
        assert p.body.contains('!Bedrijfsgegevens')
        assert p.body.contains('!Logo')
    }
}
