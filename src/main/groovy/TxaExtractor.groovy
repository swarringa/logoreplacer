import java.nio.file.Paths
import static SectionMark.*

/**
 * Extracts specific content from a txa file
 */
class TxaExtractor {
    private Reader source

    private final static EOL = System.lineSeparator()

    static {
        ClarionStringMixins.initialize()
    }

    TxaExtractor(String txaFile){
        this.source = new File(Paths.get(txaFile).normalize().toString()).newReader()
    }

    TxaExtractor(File txaFile) {
        this.source = txaFile.newReader()
    }

    TxaExtractor(StringBuffer contents){
        this.source = new StringReader(contents.toString())
    }

    /**
     * Extracts the individual procedures from a txa file
     * @param txaFile - TXA File instance
     * @return List of Procedure instances
     */
    List<Procedure> extractProcedures() {

        // Current line number (for debugging and reporting)
        def lnr = 0

        List<Procedure> procedures = []
        Procedure currentProcedure = null

        SectionMark currentSection = null
        List<SectionMark> parentSections = []
        SectionMark currentProcedureSection = null

        // [PROCEDURE] is also a child of [DEFINITION] to represent a procedure call
        // This check will affirm we're dealing with a procedure declaration
        def isProcedureDeclaration = { section ->
            section == PROCEDURE && !parentSections.contains(DEFINITION)
        }

        // A [PROCEDURE] section is considered closed when it's section mark is not the current section
        // nor a parent of the current section
        def procedureSectionClosed = { section ->
            section == null ? true : !parentSections.contains(section) && currentSection != section
        }

        this.source.eachLine { line ->

            lnr++

            if ( line.isSectionMark()){
                if ( line.isSectionEnd() ){
                    if (currentProcedure) {
                        currentSection = rollupClosedSections(parentSections, currentSection)

                        // When procedure section has been removed the current procedure definition ends
                        if ( currentProcedure && procedureSectionClosed(currentProcedureSection) ){
                            procedures << currentProcedure
                            currentProcedure = null
                        }

                        if (currentProcedure){
                            currentProcedure.body << line << EOL
                        }
                    }
                } else {
                    SectionMark section = line.asSectionMark()

                    if ( currentSection && currentSection.hasChild(section)) {
                        parentSections.push(currentSection)
                    } else {
                        // Roll up to the parent section for this section marker
                        while (!parentSections.isEmpty() && !parentSections.last().hasChild(section)) {
                            parentSections.pop()
                        }
                        // When procedure parent has been removed the current procedure definition ends
                        if ( currentProcedure && procedureSectionClosed(currentProcedureSection) ){
                            procedures << currentProcedure
                            currentProcedure = null
                        }
                    }

                    currentSection = section

                    /* Start of new Procedure definition */
                    if ( isProcedureDeclaration(section)){
                        if ( currentProcedure){
                            procedures << currentProcedure
                        }
                        currentProcedure = new Procedure()
                        currentProcedure.lineNumber = lnr
                        currentProcedureSection = section
                    }

                    if(currentProcedure){
                        currentProcedure.body << line << EOL
                    }
                }
            } else if (currentProcedure) {
                if ( isProcedureDeclaration(currentSection) ){
                    processProcedureAttribute(currentProcedure, line)
                }

                currentProcedure.body << line << EOL
            }
        }

        if (currentProcedure) {
            procedures << currentProcedure
        }

        procedures
    }

    /**
     * Remove all parent sections that need to be closed for the current [END] marker
     * @param parentSections
     * @param currentSection
     * @return
     */
    def rollupClosedSections(List<SectionMark> parentSections, SectionMark currentSection) {
        while (!parentSections.isEmpty() && !currentSection.requiresExplicitEnd()) {
            currentSection = parentSections.pop()
        }
        // currentSection now is the section matching this [END] marker
        if (!parentSections.isEmpty()) {
            currentSection = parentSections.pop()
        } else {
            currentSection = null
        }

        currentSection
    }

    /**
     * Extract procedure attributes from current line and update procedure instance
     * @param procedure
     * @param line
     * @return
     */
    def processProcedureAttribute(Procedure procedure, String line) {
        final NAME_DECL = ~/^NAME\s+(.*)\s*$/
        final TEMPLATE_DECL = ~/^FROM\s+(\w[\w\s]+)\s*$/

        if (!procedure.name) {
            (line =~ NAME_DECL).each {
                _, procedureName -> procedure.name = procedureName
            }
        }
        if (!procedure.template) {
            (line =~ TEMPLATE_DECL).each {
                _, templateName -> procedure.template = templateName
            }
        }
    }
}
