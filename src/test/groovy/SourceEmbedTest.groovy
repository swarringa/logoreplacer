class SourceEmbedTest extends GroovyTestCase implements TxaTestSupport {
    void setUp() {
        super.setUp()
        ClarionStringMixins.initialize()
        ClarionDateMixins.initialize()
    }

    /**
     * When no embed point exists the %WindowManagerMethodCodeSection embed point will
     * be added with the source embeds at the correct embed point instances
     */
    void testWindowManagerEmbedPointWithSourcesAddedWhenNoEmbeds(){
        def contents = '''\
            [EMBED]
            [END]
        '''.stripIndent()

        def p = new Procedure()
        p.name = "Test"
        p.body = new StringBuilder(contents)
        assertSectionsClosedCorrectly(p)
        def lr = new LogoReplacer()
        lr.replaceProcedureLogo(p)
        assertSectionsClosedCorrectly(p)
        assert p.body.contains('EMBED %WindowManagerMethodCodeSection')
        assertStructuresAtLine(p,1,
                windowManagerCodeSectionEmbedPoint,
                windowInitEmbedPoint,
                openBedrijfsGevens,
                ["[END]","[END]"],
                openReportEmbedPoint,
                setupLogo,
                ["[END]","[END]"]
        )
    }

    void testWindowManagerEmbedPointAddedWhenMissing(){
        def contents = '''\
        [EMBED]
            EMBED %ProcessManagerMethodCodeSection
            [INSTANCES]
                WHEN 'TakeRecord'
                [DEFINITION]
                    [GROUP]
                        INSTANCE 3
                [END]
            [END]
        [END]
        '''.stripIndent()

        def p = new Procedure()
        p.name = "Test"
        p.body = new StringBuilder(contents)
        assertSectionsClosedCorrectly(p)
        def lr = new LogoReplacer()
        lr.replaceProcedureLogo(p)
        assertSectionsClosedCorrectly(p)
        assert p.body.contains('EMBED %WindowManagerMethodCodeSection')
        assertStructuresAtLine(p,9,
                windowManagerCodeSectionEmbedPoint,
                windowInitEmbedPoint,
                openBedrijfsGevens,
                ["[END]","[END]"],
                openReportEmbedPoint,
                setupLogo,
                ["[END]","[END]"]
        )
    }

    /**
     * When the %WindowManagerMethodCodeSection embed point already exists but without any
     * instances the source embeds will be added to theexisting embed point at the correct
     * instance locations
     */
    void testSourcesAddedToEmptyWindowManagerEmbedPoint(){
        def contents = '''\
            [EMBED]
              EMBED %WindowManagerMethodCodeSection
               [INSTANCES]
               [END]
            [END]
        '''.stripIndent()

        def p = new Procedure()
        p.name = "Test"
        p.body = new StringBuilder(contents)
        assertSectionsClosedCorrectly(p)

        def lr = new LogoReplacer()
        lr.replaceProcedureLogo(p)
        assertSectionsClosedCorrectly(p)

        assert p.body.contains('!Bedrijfsgegevens')
        assert p.body.contains('!Logo')

        assertStructuresAtLine(p,1,
                windowManagerCodeSectionEmbedPoint,
                windowInitEmbedPoint,
                openBedrijfsGevens,
                ["[END]","[END]"],
                openReportEmbedPoint,
                setupLogo,
                ["[END]","[END]"]
        )
    }

    /**
     * When the %WindowManagerMethodCodeSection embed point already exists but without any
     * instances the source embeds will be added to the existing embed point at the correct
     * instance locations
     */
    void testSourcesAddedToExistingEmptyWindowManagerEmbedPointInstances(){
        def contents = '''\
            [EMBED]
              EMBED %WindowManagerMethodCodeSection
              [INSTANCES]
              [END]
            [END]
        '''.stripIndent()

        def p = new Procedure()
        p.name = "Test"
        p.body = new StringBuilder(contents)
        assertSectionsClosedCorrectly(p)

        def lr = new LogoReplacer()
        lr.replaceProcedureLogo(p)
        assertSectionsClosedCorrectly(p)

        assert p.body.contains('!Bedrijfsgegevens')
        assert p.body.contains('!Logo')

        assertStructuresAtLine(p,1,
                windowManagerCodeSectionEmbedPoint,
                windowInitEmbedPoint,
                openBedrijfsGevens,
                ["[END]","[END]"],
                openReportEmbedPoint,
                setupLogo,
                ["[END]","[END]"]
        )
    }

    /**
     * When The embed point and the instances already exists the source embeds
     * will be added to the appropriate existing instance definitions
     */
    void testSourcesAddedToCorrectExistingEmbedPointInstances(){
        def contents = """\
            [EMBED]
             EMBED %WindowManagerMethodCodeSection
             [INSTANCES]
               WHEN 'Init'
               [INSTANCES]
                 WHEN '(),BYTE'
                 [DEFINITION]
                 [END]
               [END]
               WHEN 'OpenReport'
               [INSTANCES]
                WHEN '(),BYTE'
                [DEFINITION]
                [END]
               [END]
             [END]
            [END]           
        """.stripIndent()


        def p = new Procedure()
        p.name = "Test"
        p.body = new StringBuilder(contents)
        assertSectionsClosedCorrectly(p)

        def lr = new LogoReplacer()
        lr.replaceProcedureLogo(p)
        assertSectionsClosedCorrectly(p)

        assert p.body.contains('!Bedrijfsgegevens')
        assert p.body.contains('!Logo')

        assertStructuresAtLine(p,1,
                windowManagerCodeSectionEmbedPoint,
                windowInitEmbedPoint,
                openBedrijfsGevens,
                ["[END]","[END]"],
                openReportEmbedPoint,
                setupLogo,
                ["[END]","[END]"]
        )
    }

    /**
     * When existing instance definitions already exists the source embeds will be
     * added to the end of the appropriate [DEFINITION] section. This is necessary
     * because we need to inspect the existing embeds first to see if the bedrijfsgegevens
     * file is opened
     */
    void testSourcesAddedAfterExistingDefinitions(){
        def contents = """\
            [EMBED]
             EMBED %WindowManagerMethodCodeSection
             [INSTANCES]
               WHEN 'Init'
               [INSTANCES]
                 WHEN '(),BYTE'
                 [DEFINITION]
                    [PROCEDURE]
                       procedureA()
                 [END]
               [END]
               WHEN 'OpenReport'
               [INSTANCES]
                WHEN '(),BYTE'
                [DEFINITION]
                   [GROUP]
                     INSTANCE 4
                [END]
               [END]
             [END]
            [END]           
        """.stripIndent()


        def p = new Procedure()
        p.name = "Test"
        p.body = new StringBuilder(contents)
        assertSectionsClosedCorrectly(p)

        def lr = new LogoReplacer()
        lr.replaceProcedureLogo(p)
        assertSectionsClosedCorrectly(p)

        assert p.body.contains('!Bedrijfsgegevens')
        assert p.body.contains('!Logo')

        assertStructuresAtLine(p,1,
                windowManagerCodeSectionEmbedPoint,
                windowInitEmbedPoint,
                ["[PROCEDURE]" ,"procedureA()"],
                openBedrijfsGevens,
                ["[END]","[END]"],
                openReportEmbedPoint,
                setupLogo,
                ["[GROUP]","INSTANCE 4"],
                ["[END]","[END]"]
        )
    }

    /**
     * When the embed point contains other instance locations these are
     * ignored and the sources will be added to the correct instance
     * locations
     */
    void testSourcesAddedToCorrectInstanceWhenMultipleInstances(){
        def contents = """\
            [EMBED]
                EMBED %WindowManagerMethodCodeSection
                [INSTANCES]
                    WHEN 'A1'
                        [INSTANCES]
                            WHEN 'A11'
                            [DEFINITION]
                                [PROCEDURE]
                                    SomeProcedureCall()
                            [END]
                        [END]
                    WHEN 'Init'
                       [INSTANCES]
                         WHEN '(),BYTE'
                           [DEFINITION]
                           [END]
                       [END]
                    WHEN 'A2'
                        [DEFINITION]
                           [GROUP]
                             INSTANCE 3
                        [END]
                    WHEN 'OpenReport'
                       [INSTANCES]
                          WHEN '(),BYTE'
                            [DEFINITION]
                            [END]
                       [END]
                    WHEN 'A3'
                        [DEFINITION]
                           [GROUP]
                             INSTANCE 5
                        [END]
                [END]
           [END]           
        """.stripIndent()


        def p = new Procedure()
        p.name = "Test"
        p.body = new StringBuilder(contents)
        assertSectionsClosedCorrectly(p)

        def lr = new LogoReplacer()
        lr.replaceProcedureLogo(p)
        assertSectionsClosedCorrectly(p)

        assert p.body.contains('!Bedrijfsgegevens')
        assert p.body.contains('!Logo')

        assertStructuresAtLine(p,1, windowManagerCodeSectionEmbedPoint)

        assertStructuresAtLine(p,11,
                windowInitEmbedPoint,
                openBedrijfsGevens,
                ["[END]","[END]"]
        )

        assertStructuresAtLine (p, 11 + windowInitEmbedPoint.size() + openBedrijfsGevens.size() + 2 + 5,
                openReportEmbedPoint,
                setupLogo,
                ["[END]", "[END]"]
        )
    }

    void testSourcesAddedToWindowManagerExistingInstances(){
        def contents = '''\
            [EMBED]
              EMBED %WindowManagerMethodCodeSection
              [INSTANCES]
                WHEN 'Init'
                [INSTANCES]
                  WHEN '(),BYTE'
                  [DEFINITION]
                  [END]
                [END]
                WHEN 'OpenReport'
                [INSTANCES]
                  WHEN '(),BYTE'
                  [DEFINITION]
                  [END]
                [END]
              [END]
            [END]
        '''.stripIndent()

        def p = new Procedure()
        p.name = "Test"
        p.body = new StringBuilder(contents)
        assertSectionsClosedCorrectly(p)

        def lr = new LogoReplacer()
        lr.replaceProcedureLogo(p)
        assertSectionsClosedCorrectly(p)

        assert p.body.contains('!Bedrijfsgegevens')
        assert p.body.contains('!Logo')

        assertStructuresAtLine(p,1,
                windowManagerCodeSectionEmbedPoint,
                windowInitEmbedPoint,
                openBedrijfsGevens,
                ["[END]","[END]"],
                openReportEmbedPoint,
                setupLogo,
                ["[END]","[END]"]
        )
    }

    /**
     * When only the embed point instance for logo setup is missing
     * it will be created at the end of the %WindowManagerMethodCodeSection
     * [INSTANCES] section
     */
    void testLogoSetupEmbedPointInstanceCreatedForExistingEmbedPoint(){
        def contents = """\
            [EMBED]
             EMBED %WindowManagerMethodCodeSection
             [INSTANCES]
               WHEN 'Init'
               [INSTANCES]
                 WHEN '(),BYTE'
                 [DEFINITION]
                 [END]
               [END]
             [END]
            [END]           
        """.stripIndent()

        def p = new Procedure()
        p.name = "Test"
        p.body = new StringBuilder(contents)
        assertSectionsClosedCorrectly(p)

        def lr = new LogoReplacer()
        lr.replaceProcedureLogo(p)
        assertSectionsClosedCorrectly(p)

        assert p.body.contains('!Bedrijfsgegevens')
        assert p.body.contains('!Logo')

        assertStructuresAtLine(p,1,
                windowManagerCodeSectionEmbedPoint,
                windowInitEmbedPoint,
                openBedrijfsGevens,
                ["[END]","[END]"],
                openReportEmbedPoint,
                setupLogo,
                ["[END]","[END]"]
        )
    }


    /**
     * The source embeds will be added to simple embed points without
     * instances
     */
    void testEmbedpointAddedAfterEmbedWithSingleDefinition(){
        def contents = '''\
        [EMBED]
            EMBED %BeforePrintPreview
            [DEFINITION]
            [END]
        [END]
        '''.stripIndent()


        def p = new Procedure()
        p.name = "Test"
        p.body = new StringBuilder(contents)
        assertSectionsClosedCorrectly(p)

        def lr = new LogoReplacer()
        lr.replaceProcedureLogo(p)
        assertSectionsClosedCorrectly(p)

        assert p.body.contains('!Bedrijfsgegevens')
        assert p.body.contains('!Logo')

    }

    /**
     * When the procedure already fetches the bedrijfsgegevens record
     * the code for openening the bedrijfsgegevens file will be omitted
     * and only the source embed for the logo will be inserted
     */
    void testOpenBedrijfsgegevensOmittedIfFileAlreadyFetched(){
        def contents = """\
            [EMBED]
             EMBED %WindowManagerMethodCodeSection
             [INSTANCES]
               WHEN 'Init'
               [INSTANCES]
                 WHEN '(),BYTE'
                 [DEFINITION]
                    [SOURCE]
                      PROPERTY:BEGIN
                      PRIORITY 501
                      PROPERTY:END
                      CLEAR(BED:record)
                      BED:Bedrijfsnummer = 1
                      Access:Bedrijfsgegevens.Fetch(BED:KeyBedrijfsnummer)
                 [END]
               [END]
             [END]
            [END]           
        """.stripIndent()


        def p = new Procedure()
        p.name = "Test"
        p.body = new StringBuilder(contents)
        assertSectionsClosedCorrectly(p)

        def lr = new LogoReplacer()
        lr.replaceProcedureLogo(p)
        assertSectionsClosedCorrectly(p)

        assert !p.body.contains('!Bedrijfsgegevens')
        assert p.body.contains('!Logo')

        assertStructuresAtLine(p,1,
          windowManagerCodeSectionEmbedPoint,
          windowInitEmbedPoint,
          [
              '[SOURCE]',
              'PROPERTY:BEGIN',
              'PRIORITY 501',
              'PROPERTY:END',
              'CLEAR(BED:record)',
              'BED:Bedrijfsnummer = 1',
              'Access:Bedrijfsgegevens.Fetch(BED:KeyBedrijfsnummer)'
          ],
          ["[END]","[END]"],
          openReportEmbedPoint,
          setupLogo,
          ["[END]","[END]"]
        )
    }
}
