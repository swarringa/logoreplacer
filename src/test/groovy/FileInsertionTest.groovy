class FileInsertionTest extends GroovyTestCase implements TxaTestSupport {

    void setUp() {
        super.setUp()
        ClarionStringMixins.initialize()
    }


    /**
     * If Bedrijfsgegevens is present as a primary file
     * the files section is not changed
     */
    void testNoFileInsertedIfPrimaryFilePresent(){
        def contents = '''\
            [FILES]
            [PRIMARY]
            Bedrijfsgegevens
            [INSTANCE]
            0
            [KEY]
            BED:KeyBedrijfsnummer
            [PROMPTS]\
        '''.trimLines() // Remove leading/trailing spaces for == below

        def p = new Procedure()
        p.name = "Test"
        p.body = new StringBuilder(contents)
        assertSectionsClosedCorrectly(p)

        def lr = new LogoReplacer()
        lr.replaceProcedureLogo(p)
        assertSectionsClosedCorrectly(p)

        // No actions taken
        assert contents == p.body.toString()
    }


    /**
     * If Bedrijfsgegevens is present as a secondary file
     * the [FILES]] section is not modifed
     */
    void testNoFileInsertedIfSecondaryFilePresent(){
        def contents = '''\
            [FILES]
            [PRIMARY]
            DebOmzetten
            [INSTANCE]
            0
            [KEY]
            DOMZ:KeyDebiteur
            [SECONDARY]
            Bedrijfsgegevens DebOmzetten
            [PROMPTS]\
        '''.trimLines() // Remove leading/trailing spaces for == below

        def p = new Procedure()
        p.name = "Test"
        p.body = new StringBuilder(contents)
        assertSectionsClosedCorrectly(p)

        def lr = new LogoReplacer()
        lr.replaceProcedureLogo(p)
        assertSectionsClosedCorrectly(p)

        // No actions taken
        assert contents == p.body.toString()
    }

    /**
     * When the [FILES] section already contains Bedrijfsgegevens as
     * an other file no file will be added
     */
    void testNoFileInsertedIfOtherFilePresent(){
        def contents = '''\
            [FILES]
            [PRIMARY]
            DebOmzetten
            [INSTANCE]
            0
            [KEY]
            DOMZ:KeyDebiteur
            [SECONDARY]
            Debiteuren DebOmzetten
            [OTHERS]
            DebMailgroep
            MailgroepDeb
            Bedrijfsgegevens
            [PROMPTS]\
        '''.trimLines() // Remove leading/trailing spaces for == below

        def p = new Procedure()
        p.name = "Test"
        p.body = new StringBuilder(contents)
        assertSectionsClosedCorrectly(p)

        def lr = new LogoReplacer()
        lr.replaceProcedureLogo(p)
        assertSectionsClosedCorrectly(p)

        // No actions taken
        assert contents == p.body.toString()
    }

    /**
     * When Bedrijfsgegevens is not declared as a primary or
     * secondary file and the [OTHERS] section is missing
     * a new [OTHERS] section is added with Bedrijfsgegevens
     */
    void testOtherSectionAddedWhenNoPrimaryAndSecondaryBedrijfsgegevens() {
        def contents = '''\
            [FILES]
            [PRIMARY]
            DebOmzetten
            [INSTANCE]
            0
            [KEY]
            DOMZ:KeyDebiteur
            [SECONDARY]
            Debiteuren DebOmzetten
            [PROMPTS]\
        '''.stripIndent()

        def p = new Procedure()
        p.name = "Test"
        p.body = new StringBuilder(contents)
        assertSectionsClosedCorrectly(p)

        def lr = new LogoReplacer()
        lr.replaceProcedureLogo(p)
        assertSectionsClosedCorrectly(p)

        // No actions taken
        assertStructuresAtLine(p, 9,
            [
                "[OTHERS]",
                "Bedrijfsgegevens",
                "[PROMPTS]"
            ]
        )
    }

    /**
     * When Bedrijfsgegevens is not declared as a primary or
     * secondary file but the [OTHERS] section is present and does
     * not contain the Bedrijfsgegevens file the file is appended
     * to the [OTHERS] section
     */
    void testOtherSectionUpdatedWhenNoPrimaryAndSecondaryBedrijfsgegevens() {
        def contents = '''\
            [FILES]
            [PRIMARY]
            DebOmzetten
            [INSTANCE]
            0
            [KEY]
            DOMZ:KeyDebiteur
            [OTHERS]
            Debiteuren
            [PROMPTS]\
        '''.stripIndent()

        def p = new Procedure()
        p.name = "Test"
        p.body = new StringBuilder(contents)
        assertSectionsClosedCorrectly(p)

        def lr = new LogoReplacer()
        lr.replaceProcedureLogo(p)
        assertSectionsClosedCorrectly(p)

        // No actions taken
        assertStructuresAtLine(p, 7,
                [
                        "[OTHERS]",
                        "Debiteuren",
                        "Bedrijfsgegevens",
                        "[PROMPTS]"
                ]
        )
    }
}
